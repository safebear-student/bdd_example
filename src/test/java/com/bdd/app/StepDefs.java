package com.bdd.app;

import com.bdd.app.pages.ConfigurePage;
import com.bdd.app.pages.LoginPage;
import com.bdd.app.pages.ReportingPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 06/12/2017.
 */
public class StepDefs {

    WebDriver driver;
    String url;
    LoginPage loginPage;
    ConfigurePage configurePage;
    ReportingPage reportingPage;

    @Before
    public void setUp(){

        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        configurePage = new ConfigurePage(driver);
        reportingPage = new ReportingPage(driver);

        url = "http://simplerisk.local/index.php";
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){
        try {
            reportingPage.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^the following users are in the system:$")
    public void the_following_users_are_in_the_system(DataTable userdetails) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        List<List<String>> data = userdetails.raw();
        // Check to see if the user exists
        loginPage.enterUsername("admin");
        loginPage.enterPassword("^Y&U8i9o0p");
        loginPage.login();
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        if (!configurePage.checkIfUserIsPresent(data.get(0).get(0))){
            configurePage.enterNewUsername(data.get(0).get(0));
            configurePage.enterNewPassword(data.get(0).get(2));
            configurePage.givePermissions(data.get(0).get(1));
            configurePage.createUser();

            System.out.println("User created");
            configurePage.logout();
        }
        else {
            configurePage.logout();
        }



        //throw new PendingException();
    }


    @Given("^a user called (.+) with (.+) permissions and password (.+)$")
    public void a_user_called_simon_with_administrator_permissions_and_password_S_feB_ar(String username, String permissions, String password) throws Throwable {
        // Check to see if the user exists
        loginPage.enterUsername("admin");
        loginPage.enterPassword("^Y&U8i9o0p");
        loginPage.login();
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        if (!configurePage.checkIfUserIsPresent(username)){
            configurePage.enterNewUsername(username);
            configurePage.enterNewPassword(password);
            configurePage.givePermissions(permissions);
            configurePage.createUser();

            System.out.println("User created");
            configurePage.logout();
        }
        else {
            configurePage.logout();
        }
    }


    @When("^(.+) is logged in with password (.+)$")
    public void simon_is_logged_in(String user, String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.enterUsername(user);
        loginPage.enterPassword(password);
        loginPage.login();
    }

    @Then("^he is able to view a (.+)'s account$")
    public void he_is_able_to_view_a_george_s_account(String user) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        configurePage.viewUserDetails(user);
        assertTrue(configurePage.checkUserDetails(user));
    }

    @Given("^a george has lost his password$")
    public void a_george_has_lost_his_password() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^simon can reset george's password$")
    public void simon_can_reset_george_s_password() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^He creates an account for bob$")
    public void he_creates_an_account_for_bob() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^bob can login$")
    public void bob_can_login() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^He creates an account for hannah$")
    public void he_creates_an_account_for_hannah() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^hannah can login$")
    public void hannah_can_login() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^He creates an account for tom$")
    public void he_creates_an_account_for_tom() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^tom can login$")
    public void tom_can_login() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
