Feature: User Management

  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
   - Non-admin users have no permissions over other accounts
   - The administrator must be logged in to see user accounts

  Questions:
   - Do admin users have access to all accounts or only the accounts in their group?

  To do:
   - Force reset of password on account creation

  Domain language:
  CRUD = Create, Read, Update, Delete
  Administrator permissions = Access to everything
  Risk management permissions = Only access to CRUD risks

  Background:
    Given the following users are in the system:
    |simon|administrator|S@feB3ar|
    |george|risk_management|S@feB3ar|

    #a user called simon with administrator permissions and password S@feB3ar
    #And a user called george with risk_management permissions and password S@feB3ar

  @high-impact
  Scenario Outline: The administrator checks a user's details
    When simon is logged in with password S@feB3ar
    Then he is able to view a <user>'s account
    Examples:
      | user  |
      |george |

    #@high-risk
  @to-do
  Scenario Outline: A user's password is reset by the administrator
    Given a <user> has lost his password
    When simon is logged in
    Then simon can reset george's password
    Examples:
      | user |
      |george|

  @to-do
  Scenario Outline: A user is created
    Given simon is logged in
    When He creates an account for <new_user>
    Then <new_user> can login

    Examples:
      | new_user |
      |bob       |
      |hannah    |
      |tom       |

